# Atelier sur les données ouvertes


## API

## Python et dataviz


## Exemples

Source données sur l'éducation: https://ouvert.canada.ca/data/fr/dataset/b94aceeb-7aa5-4f19-9f6c-860dd5eb5327


## Sources de données intéressantes:

https://learn.g2.com/open-data-sources

- https://ouvert.canada.ca/fr

- https://www.donneesquebec.ca/

- https://www.data.gouv.fr/fr/



## api données ouvertes:

liste: [3~https://open.canada.ca/data/fr/api/3/action/package_list

doc minimaliste: https://ouvert.canada.ca/data/fr/api/1/util/snippet/api_info.html?resource_id=0b5a739d-b01e-43df-8757-7fc1453baec5#collapse-querying

batiments https://open.canada.ca/data/fr/api/3/action/datastore_search?resource_id=0b5a739d-b01e-43df-8757-7fc1453baec5&limit=5


où manger à sherbrooke https://www.donneesquebec.ca/recherche/dataset/ou-manger/resource/386e62b2-47ae-43bc-a85a-efbcaf3130ba


comment installer: 


## problèmes

- données accessibles
- données bien structurées
  - accessibles comment? On va chercher un disque si on demande
  - pdf
  - csv
  - api
  - structurer = concevoir des paradigmes épistémologiques
  - standardisé ou pas? normalisation


  TED talk: https://www.ted.com/talks/ben_wellington_how_we_found_the_worst_place_to_park_in_new_york_city_using_big_data


Policies guidelines: https://opendatapolicies.org/guidelines/


<iframe scrolling="no" class="iFrame-vimeo-iframe" src="//europa.eu/webtools/crs/iframe/?lang=fr&amp;oriurl=https://www.youtube.com/embed/_wv9QflBJIY&amp;list=PLT5rARDev_rnG9rj7hZG0hALPWQS8mLGA&amp;index=4" data-height-large="508" data-height-medium="350" data-height-small="250" data-width-large="905" data-width-medium="500" data-width-small="300" style="width: 905px; height: 508px;">
      </iframe>
