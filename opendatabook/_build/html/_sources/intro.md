# Informations sur l'atelier

Cet atelier est consacré aux enjeux de l’ouverture des données et des contenus en contexte académique et patrimonial. Depuis la fin des années 90, la communauté universitaire et la société civile ont réclamé l’émergence de communs de la connaissance. La promotion de l’accès libre (open access), des données ouvertes (open data), et des contenus ouverts (open content) ont profondément fait évoluer les manières de travailler des chercheurs ou des organismes culturels.



Au terme de l’atelier, l’étudiant sera en mesure de

• analyser de manière critique les politiques d’ouverture
• évaluer et sélectionner une licence libre appropriée à un projet
• développer une stratégie de publication adaptée à leur projet

## Rendus

Voici deux rendus des étudiants:

- [Stations Bixi, bon pain - mauvais pain, et lieux culturels](http://vitalirosati.net/slides/opendata/HNU6055_v01_2.html)
- [Exploratio0n données Covid](http://vitalirosati.net/slides/opendata/covid_v4.html)


## Évaluation

Les étudiant.e.s seront divisé.es en équipes.

Chaque équipe devra réaliser un petit projet d'utilisation de données ouvertes. 


Le projet pourra être réalisé avec un Jupyter-notebook et devra inclure:

- Une courte présentation du projet et de la question qu'on veut poser
- Une partie de récupération des données
- Une partie de normalisation des données (si nécessaire)
- Une partie d'exploitation des données: les données répondent à la question


## Préparer sa machine

Nous allons utiliser python et des jupyter-notebook pour explorer des données. 

Il faudra donc installer python et jupyter-notebook sur vos machines. Si vous n'avez aucune idée de comment faire, vous pouvez suivre les instructions ici: https://melaniewalsh.github.io/Intro-Cultural-Analytics/02-Python/01-Install-Python.html (et installer anaconda) 


## Sujets traités

### 13-01-2023

- [Que sont les données ouvertes?](./theorie.md)
- [Principes et problèmes](principes.md) 
- [Licences](licences.md)
- [Introduction à python](intropython.ipynb)
- [Introduction aux jupyter-notebooks](introjupyternb.ipynb)
- Travail en atelier: premier brain storming sur des idées de projet

### 20-01-2023

- [Introduction à pandas](pandas.ipynb)
- [Un premier exemple](donneesEducation.ipynb)
- [Utiliser une api](api.ipynb) 
- [Un exemple](ecoles.ipynb)
- Travail en atelier: présentation des projets et de leurs enjeux théoriques et techniques

### 27-01-2023

- Travail en atelier


## Resources 


[Manuel de l'open data de l'OKF](https://opendatahandbook.org/guide/fr/)

[Liste de resources de l'OKF](https://opendatahandbook.org/resources/)

## Des plateformes de données ouvertes 


- https://ouvert.canada.ca/fr

- https://www.donneesquebec.ca/

- https://www.data.gouv.fr/fr/

- https://www.data.gov/

- https://data.gov.uk/

- http://data.europa.eu/euodp/en/data/

- https://www.opendatanetwork.com/

- https://data.unicef.org/

- https://www.who.int/gho/database/en/

- https://www.opensciencedatacloud.org/

- https://pds.nasa.gov/

- https://earthdata.nasa.gov/

- https://wikidata.org/

- https://datasetsearch.research.google.com/

- https://research.google/tools/datasets/
