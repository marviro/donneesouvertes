#!/usr/bin/env python
# coding: utf-8

# ### Les API
# 
# Une API (application programming interface) est une structure normalisée qui permet d'interagir avec une application. Nous nous intéresserons plus particulièrement à des API qui mettent à disposition des données.
# 
# Je vais donner trois exemples:
# 
# - l'api de zotero
# - l'api de wikidata
# - l'api de l'Anthologie Palatine
# 
# ### Json
# 
# Json est un format de structuration des données très simple.
# 
# 
# Par exemple, pour structurer les clés-valeurs suivantes:
# 
# Article:
# - titre
# - auteur
# - corps
# 
# en xml il faudrait:
# 
# ```
# <article>
#     <titre>Montitre</titre>
#     <auteur>Thomas</auteur>
#     <corps>Mon texte</corps>
# </article>
# ```
# 
# en json:
# 
# ```
# {
# "titre":"Mon titre",
# "auteur":"Thomas",
# "corps":"Mon texte,
# }
# ```
# 
# Regardons un exemple du json de zotero:
# 
# ```json
# 
# {
# 
#     "key": "P4UXP443",
#     "version": 1380,
#     "library": {
#         "type": "group",
#         "id": 322999,
#         "name": "Écritures numériques",
#         "links": {
#             "alternate": {
#                 "href": "https://www.zotero.org/groups/critures_numriques",
#                 "type": "text/html"
#             }
#         }
#     },
#     "links": {
#         "self": {
#             "href": "https://api.zotero.org/groups/322999/items/P4UXP443",
#             "type": "application/json"
#         },
#         "alternate": {
#             "href": "https://www.zotero.org/groups/critures_numriques/items/P4UXP443",
#             "type": "text/html"
#         }
#     },
#     "meta": {
#         "createdByUser": {
#             "id": 2755709,
#             "username": "laurentdm",
#             "name": "",
#             "links": {
#                 "alternate": {
#                     "href": "https://www.zotero.org/laurentdm",
#                     "type": "text/html"
#                 }
#             }
#         },
#         "lastModifiedByUser": {
#             "id": 3449834,
#             "username": "enricoagomarchese",
#             "name": "Enrico Agostini-Marchese",
#             "links": {
#                 "alternate": {
#                     "href": "https://www.zotero.org/enricoagomarchese",
#                     "type": "text/html"
#                 }
#             }
#         },
#         "creatorSummary": "Carrier-Lafleur et al.",
#         "parsedDate": "2018-04-09",
#         "numChildren": 0
#     },
#     "citation": "<span>Carrier-Lafleur et al., “L’invention littéraire des médias.”</span>",
#     "data": {
#         "key": "P4UXP443",
#         "version": 1380,
#         "itemType": "journalArticle",
#         "title": "L'invention littéraire des médias",
#         "creators": [
#             {
#                 "creatorType": "author",
#                 "firstName": "Thomas",
#                 "lastName": "Carrier-Lafleur"
#             },
#             {
#                 "creatorType": "author",
#                 "firstName": "André",
#                 "lastName": "Gaudreault"
#             },
#             {
#                 "creatorType": "author",
#                 "firstName": "Servanne",
#                 "lastName": "Monjour"
#             },
#             {
#                 "creatorType": "author",
#                 "firstName": "Marcello",
#                 "lastName": "Vitali-Rosati"
#             }
#         ],
#         "abstractNote": "Les médias existeraient-ils sans la littérature ? Pourrait-on parler de « télévision », de « photographie », de « cinéma » ou du « numérique » sans que ces dispositifs aient aussi été construits, institutionnalisés et même parfois déconstruits dans l’imaginaire collectif par la littérature et son discours ? À l’heure où le numérique semble encore s’inventer, le présent dossier vise à souligner le rôle du fait littéraire dans la construction de nos médias. En même temps, l’hybridation médiatique de notre contemporanéité numérique rend nécessaire une réflexion sur la capacité des médias à se réinventer réciproquement, renouvelant chaque fois l’ordre du discours et la fonction de la littérature. En raison de sa capacité à témoigner de l’hétérogénéité de notre univers médiatique, la littérature offre un terrain privilégié – où tout reste encore à faire – pour mener une telle recherche.",
#         "publicationTitle": "Sens Public",
#         "volume": "",
#         "issue": "",
#         "pages": "",
#         "date": "2018-04-09",
#         "series": "",
#         "seriesTitle": "",
#         "seriesText": "",
#         "journalAbbreviation": "",
#         "language": "fr",
#         "DOI": "",
#         "ISSN": "2104-3272",
#         "shortTitle": "",
#         "url": "https://papyrus.bib.umontreal.ca/xmlui/handle/1866/19962",
#         "accessDate": "2018-04-20T18:46:45Z",
#         "archive": "",
#         "archiveLocation": "",
#         "libraryCatalog": "sens-public.org",
#         "callNumber": "",
#         "rights": "http://creativecommons.org/licenses/by-nc-sa/4.0/",
#         "extra": "",
#         "tags": [
#             {
#                 "tag": "Circulation des contenus"
#             },
#             {
#                 "tag": "Imaginaire-réel"
#             },
#             {
#                 "tag": "Intermédialité"
#             },
#             {
#                 "tag": "Littérature"
#             },
#             {
#                 "tag": "Légitimation des contenus"
#             },
#             {
#                 "tag": "Médias"
#             },
#             {
#                 "tag": "Production des contenus"
#             }
#         ],
#         "collections": [
#             "P4PRNB6V",
#             "V6D6F4ZX",
#             "C9RJ5F8P"
#         ],
#         "relations": { },
#         "dateAdded": "2018-04-20T18:46:45Z",
#         "dateModified": "2019-09-26T14:14:38Z"
#     }
# 
# }
# 
# ```

# In[1]:


import json
import requests


# In[2]:


url_api = 'https://api.zotero.org/'
group_number = '322999'
item_id = 'P4UXP443'
parameters = {
    'include': 'citation,data'
}
url = url_api +'groups/'+group_number+'/items/'+item_id+'/'
data = requests.get(url,parameters).json()

data['citation']


# En utilisant une api il est possible par exemple d'afficher ces données dans une forme particulière sur un site. Par exemple ici: https://ecrituresnumeriques.ca/fr/2018/4/9/spanThomas-Carrier-Lafleur-Andre-Gaudreault-Servanne-Monjouret-al-Linvention-litteraire-des-medias-iSens-Publici-avril-twozerooneeightspan

# In[3]:


data['data']['creators'][0]['firstName']


# ## Un exemple d'utilisation de Wikidata
# 
# Nous allons utiliser wikidata pour afficher des informations à propos du livre [L'agir en condition hyperconnectée](http://parcoursnumeriques-pum.ca/11-agir/index.html).
# 
# Le livre cite des personnalités et l'HTML est bien structuré: l'éditeur a pensé à associer les personnes citées à leur identifiant wikidata.
# 
# Par exemple, dans le [chapitre1](http://parcoursnumeriques-pum.ca/11-agir/chapitre1.html) on parle de Denis Coderre et l'html renseigne son identifiant wikidata https://www.wikidata.org/wiki/Q3022603.
# 
# Cela nous permet d'interroger l'api de wikidata et d'aller voir toutes les informations que wikidata peut nous donner sur Coderre.
# Parmi ces informations il y a son pays de naissance - qui a aussi un id. Et ce pays a des coordonnées géographiques.
# 
# <span data-idwiki="https://www.wikidata.org/wiki/Q3022603" class="personnalite">Denis Coderre</span>
# 
# Voici les endpoints de l'api:
# 
# https://www.wikidata.org/w/api.php?action=wbgetentities&sites=frwiki&ids=Q3022603&format=json
# https://www.wikidata.org/w/api.php?action=wbgetentities&sites=frwiki&ids=Q&format=json
# 
# 

# In[4]:


from lxml import html
import requests
import json
from urllib.request import urlopen
import sys
get_ipython().system('{sys.executable} -m pip install folium')
import folium
from lxml import etree


# In[5]:


chapitres = ['introduction', 'chapitre1']


# In[6]:


geodata=[]
cadata=[]
countries=[]
for chapitre in chapitres:
    url1 = str(url)+str(chapitre)+'.html'
    print(chapitre)
    #data = etree.HTML(urlopen(url1).read()) avec ça ça marche plus
    response = requests.get(url1,headers={"User-Agent": "XY"}) # erreur si on ne spécifie pas l'user-agent
    data = response.content.decode("utf-8")
    data = etree.HTML(data)
    ids=data.xpath('//html//span[@class="personnalite"]/@data-idwiki')
    wikidata=[]
    ids=set(ids)
    for id  in ids:
        cdata={}
        id=id.replace('https://www.wikidata.org/wiki/', '')
    
        r = requests.get('https://www.wikidata.org/w/api.php?action=wbgetentities&sites=frwiki&ids='+id+'&format=json')
        r=json.loads(r.text)
        wikidata.append(r)
        label = r['entities'][str(id)]['labels']
        try:
            if label['en']:
                name=label['en']['value']
                print(name)
            elif label['fr']:
                name=label['fr']['value']
                print(name)
            else:
                print('Mais comment il s\'appelle c\'lui là???')
            # for prop in r['entities'][str(id)]['claims']['P27']:
            countryid=r['entities'][str(id)]['claims']['P27'][0]['mainsnak']['datavalue']['value']['id']
            print('Pays id ' + countryid)
            countries.append(countryid)
            cdata.update({'name': name})

            cdata.update({'id': countryid})
            cadata.append(cdata)
    
        except:
            print('Marche pas... pour'+ str(id))
    # for entity in r:
        # print (entity['entities'][str(id)]['claims']['P27']['mainsnak']['datavalue']['value'])

print (countries)

print (cadata)
countries= set(countries)
for country in countries:
    print(country)
    rc = requests.get('https://www.wikidata.org/w/api.php?action=wbgetentities&sites=frwiki&ids='+country+'&format=json')
    rc = json.loads(rc.text)
    
    countryname = rc['entities'][str(country)]['labels']['fr']['value']
    print(countryname)
    countrygps = rc['entities'][str(country)]['claims']['P625']
    punkt={}
    for i in countrygps:
        latitude=i['mainsnak']['datavalue']['value']['latitude']
        print(latitude)
        longitude=i['mainsnak']['datavalue']['value']['longitude']
        print(longitude)
     
    punkt.update({"lat": latitude, "long": longitude, "name": countryname})
    text='<br>Pays de:<br> '
    for item in cadata:
        try:
            if item['id'] == country:
                print(item['name'])
                text+= str(item['name'])+ ', '
                
        except:
            continue
    punkt.update({"text": text})
    geodata.append(punkt)
print(geodata)    


# In[7]:


m = folium.Map([0, 0], zoom_start=2)

for point in geodata:
   
    folium.Marker([point['lat'], point['long']], popup=point['name']+point['text']).add_to(m)
m


# In[ ]:




