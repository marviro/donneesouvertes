#!/usr/bin/env python
# coding: utf-8

# # Atelier Données ouvertes
# 
# HNU3055 / HNU6055
# 
# **Équipe**  
# Parham Aledavood  
# Amélie Levasseur-R.
# 
# **Question**  
# [EDIT] Voir s'il y a une différence dans la proportion de personnes aux soins intensifs dû à la COVID-19 dans 3 régions du Québec : une région très urbanisée (Montréal), une moyennement et une région plus éloignée.
# 
# Voir s'il y a une relation entre le taux de vaccination et les cas et hospitalisations de COVID-19, selon les régions administratives du Québec.
# 
# **Sources des données**  
# https://www.donneesquebec.ca/recherche/dataset/covid-19-portrait-quotidien-de-la-vaccination#  
# https://www.donneesquebec.ca/recherche/dataset/covid-19-portrait-quotidien-des-hospitalisations/resource/2d8bd4f8-4715-4f33-8cb4-eefcec60a4c9
# 
# **Documents explicatifs**  
# listevariables_notesmetho_vaccination_20221107.pdf  
# listevariables_notesmetho_hospit_20220621.pdf
# 
# 
# **Problèmes rencontrés**
# 
# - Adapter le code trouvé à nos besoins spécifiques. Par exemple, nous avons eu de la difficulté à conserver les colonnes qui contiennent "Total" et aussi celle de la date. -> RÉGLÉ!
# 
# - Rendre analyse proportionnelle selon la population de chaque région. Doit trouver données sur la population par région.
# 
# - Doit revoir les données de vaccination conservées, utiliser les données par jour et non le cumul qui ne peut qu'augmenter. -> RÉGLÉ!
# 
# - Pas le même nombre de dates entre les deux jeux, même si la date de début et fin sont la même (681 observations hospitalisations et 755 observations vaccination).
# 
# - Régions des jeux sur la COVID ne sont pas exactement les mêmes que les régions administratives.

# ## Charger les librairies

# In[1]:


import pandas as pd


# ## Téléchargement des données

# In[2]:


url_vacc = "https://msss.gouv.qc.ca/professionnels/statistiques/documents/covid19/COVID19_Qc_Vaccination_RegionResidence.csv"
df_vacc = pd.read_csv(url_vacc)
# source code : https://datatofish.com/export-dataframe-to-csv/
df_vacc.to_csv(r'data/raw/COVID19_Qc_Vaccination_RegionResidence.csv')

url_hosp = "https://msss.gouv.qc.ca/professionnels/statistiques/documents/covid19/COVID19_Qc_HistoHospit.csv"
df_hosp = pd.read_csv(url_hosp)
df_hosp.to_csv(r'data/raw/COVID19_Qc_HistoHospit.csv')


# In[3]:


df_vacc


# In[4]:


df_hosp


# In[ ]:





# ## Nettoyage et manipulations des données
# 
# ### Hospitalisations

# In[5]:


# uniformiser le nom de colonne "date" qui avait une majuscule dans ce jeu
df_hosp = df_hosp.rename(columns = {"Date": "date"})

# éliminer les données d'avant 2021
df_hosp = df_hosp[df_hosp["date"].str.contains("2020") == False]

# ordonner en ordre croissant de date
df_hosp = df_hosp.sort_values("date", ascending=True)

# conserver seulement les colonnes total, en plus de la date
# source code : https://stackoverflow.com/questions/69387240/how-to-drop-columns-which-contains-specific-characters-except-one-column
df_hosp_total = df_hosp.loc[:, df_hosp.columns.str.contains("Total") | (df_hosp.columns == "date")]

df_hosp_total.to_csv(r'data/processed/COVID19_Qc_HistoHospit.csv')
df_hosp_total


# conserver seulement les colonnes soins intensifs, en plus de la date
df_hosp_si = df_hosp.loc[:, df_hosp.columns.str.contains("_Si") | (df_hosp.columns == "date")]
df_hosp_si


# ### Vaccination

# In[6]:


# éliminer les données d'avant 2021
df_vacc = df_vacc[df_vacc["date"].str.contains("2020") == False]

# ordonner en ordre croissant de date
df_vacc = df_vacc.sort_values("date", ascending=True)

# conserver seulement les données par jour, et la date
df_vacc_jour = df_vacc.loc[:, df_vacc.columns.str.contains("jour") | (df_vacc.columns == "date")]
df_vacc_jour = df_vacc_jour.drop(columns=["RSSND_DOSES_Total_jour", "RSSHQ_DOSES_Total_jour", "RSSAL_DOSES_Total_jour"])

df_vacc_jour.to_csv(r'data/processed/COVID19_Qc_Vaccination_RegionResidence.csv')
df_vacc_jour


# ## Fusionner les jeux de données 
# 
# Parce que le jeu sur la vaccination ne contient pas toutes les dates, entre autres.

# In[7]:


data = pd.merge(df_vacc_jour, df_hosp_total, how='left', on="date")

data_hosp = pd.merge(df_hosp_si, df_hosp_total, how='left', on="date")


# ### Régions
# 
# RSS06 : Montréal  
# RSS14 : Lanaudière  
# RSS11 : Gaspésie

# In[8]:


# conserver seulement les colonnes de la région, en plus de la date
data_hosp_06 = data_hosp.loc[:, data_hosp.columns.str.contains("RSS06") | (data_hosp.columns == "date")]
data_hosp_14 = data_hosp.loc[:, data_hosp.columns.str.contains("RSS14") | (data_hosp.columns == "date")]
data_hosp_11 = data_hosp.loc[:, data_hosp.columns.str.contains("RSS11") | (data_hosp.columns == "date")]


# ### Ajouter proportions soins intensifs vs total hospitalisations

# In[9]:


data_hosp_06 = data_hosp_06.assign(Proportion_Si = data_hosp_06["ACT_Si_RSS06"] / data_hosp_06["ACT_Total_RSS06"] * 100)
data_hosp_14 = data_hosp_14.assign(Proportion_Si = data_hosp_14["ACT_Si_RSS14"] / data_hosp_14["ACT_Total_RSS14"] * 100)
data_hosp_11 = data_hosp_11.assign(Proportion_Si = data_hosp_11["ACT_Si_RSS11"] / data_hosp_11["ACT_Total_RSS11"] * 100)
data_hosp_06


# ### Population
# 
# Source : Banque de données des statistiques officielles sur le Québec

# In[10]:


pop2021_06 = 2024806 / 100000
pop2021_14 = 535132 / 100000
pop2021_11 = 92015 / 100000

data_hosp_06 = data_hosp_06.assign(Proportion_Pop_Si = data_hosp_06["ACT_Si_RSS06"] / pop2021_06)
data_hosp_11 = data_hosp_11.assign(Proportion_Pop_Si = data_hosp_11["ACT_Si_RSS11"] / pop2021_11)
data_hosp_14 = data_hosp_14.assign(Proportion_Pop_Si = data_hosp_14["ACT_Si_RSS14"] / pop2021_14)

data_hosp_06 = data_hosp_06.assign(Proportion_Pop_Total = data_hosp_06["ACT_Total_RSS06"] / pop2021_06)
data_hosp_11 = data_hosp_11.assign(Proportion_Pop_Total = data_hosp_11["ACT_Total_RSS11"] / pop2021_11)
data_hosp_14 = data_hosp_14.assign(Proportion_Pop_Total = data_hosp_14["ACT_Total_RSS14"] / pop2021_14)
data_hosp_14


# In[ ]:





# ## Visualisations

# In[11]:


# sources code : 
# https://matplotlib.org/stable/tutorials/introductory/customizing.htmlimport matplotlib.pyplot as plt
# https://pandas.pydata.org/pandas-docs/version/0.23/generated/pandas.DataFrame.plot.html

import matplotlib as mpl
import matplotlib.pyplot as plt
from cycler import cycler

# paramètres par défaut pour tous nos graphiques
plt.rcParams["figure.figsize"] = (12, 4)
plt.rcParams["axes.grid"] = True
plt.rcParams["figure.titleweight"] = "bold"
mpl.rcParams['axes.prop_cycle'] = cycler(color=['r', 'g', 'b', 'y'])
mpl.rcParams["lines.linewidth"] = 1
mpl.rcParams["xtick.major.size"] = 1
#plt.rcParams.keys()
#mpl.rcParams.keys()


# In[153]:


df_hosp_total.plot(x= "date", y="ACT_Total_RSS99", title="Nombre d'hospitalisations (Québec)")
df_hosp_si.plot(x= "date", y="ACT_Si_RSS99", title="Nombre d'hospitalisations aux soins intensifs (Québec)")

df_vacc_jour.plot(x= "date", y="RSS99_DOSES_Total_jour", title="Nombre de vaccins administrés (Québec)")


# ### Proportion de personnes aux soins intensifs

# In[154]:


#data_hosp_11.plot(x= "date", y="Proportion_Pop_Si", title="Nombre de personnes aux soins intensifs (par 100 000 habitants)")

date = data_hosp_06["date"]
RSS06 = data_hosp_06["Proportion_Pop_Si"]
RSS11 = data_hosp_11["Proportion_Pop_Si"]
RSS14 = data_hosp_14["Proportion_Pop_Si"]

plt.plot(date, RSS06, label="Montréal")
plt.plot(date, RSS11, label="Gaspésie")
plt.plot(date, RSS14, label="Lanaudière")

plt.legend()
plt.show()


# ### Proportion de personnes hospitalisées

# In[164]:


date = data_hosp_06["date"]
RSS06t = data_hosp_06["Proportion_Pop_Total"]
RSS11t = data_hosp_11["Proportion_Pop_Total"]
RSS14t = data_hosp_14["Proportion_Pop_Total"]

plt.plot(date, RSS06t, label="Montréal")
plt.plot(date, RSS11t, label="Gaspésie")
plt.plot(date, RSS14t, label="Lanaudière")

plt.xlabel("Date")
plt.ylabel("Nombre par 100 000 habitants")
plt.title("Proportion de personnes hospitalisées")
plt.legend()
plt.show()


# ### Proportion de personnes hospitalisées aux soins intensifs

# In[163]:


date = data_hosp_06["date"]
RSS06tsi = data_hosp_06["Proportion_Si"]
RSS11tsi = data_hosp_11["Proportion_Si"]
RSS14tsi = data_hosp_14["Proportion_Si"]

plt.plot(date, RSS06tsi, label="Montréal")
plt.plot(date, RSS11tsi, label="Gaspésie")
plt.plot(date, RSS14tsi, label="Lanaudière")

plt.legend()
plt.show()


# ### Points à travailler
# 
# soins intensifs
# morts
# comparaison avec permissions en cours, déconfinement
# hospitalisation personnes vaccinées ou pas?
# date élection, événements
# cumul vaccination
# par semaine
# question plus précise
# loufoque : comparaison avec autre chose complètement

# In[156]:


date = data["date"]
hosp = data["ACT_Total_RSS99"]
#vacc = data["RSS99_DOSES_Total_jour"]
si = data["ACT_Si_RSS99"]

plt.plot(date, hosp, label="Nombre d'hospitalisations")
plt.plot(date, si, label="Nombre aux soins intensifs")

plt.show()


# In[ ]:




