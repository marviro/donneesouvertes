#!/usr/bin/env python
# coding: utf-8

# # Atelier Données ouvertes
# 
# HNU3055 / HNU6055
# 
# **Équipe**  
# Parham Aledavood  
# Amélie Levasseur-R.
# 
# **Question**  
# Déterminer s'il y a une différence dans la proportion de personnes hospitalisées aux soins intensifs dû à la COVID-19 dans 3 régions du Québec : une région très urbanisée (Montréal), une moyennement (Lanaudière) et une région plus éloignée (Gaspésie).
# 
# **Sources des données**  
# https://www.donneesquebec.ca/recherche/dataset/covid-19-portrait-quotidien-des-hospitalisations/resource/2d8bd4f8-4715-4f33-8cb4-eefcec60a4c9
# 
# **Documents explicatifs**  
# listevariables_notesmetho_hospit_20220621.pdf
# 
# 
# **Problèmes rencontrés**
# 
# - Adapter le code trouvé à nos besoins spécifiques. Par exemple, nous avons eu de la difficulté à conserver les colonnes qui contiennent "Total" et aussi celle de la date. -> RÉGLÉ!
# 
# - Rendre analyse proportionnelle selon la population de chaque région. Doit trouver données sur la population par région. -> RÉGLÉ!
# 
# - Régions du jeu sur les hospitalisations ne sont pas exactement les mêmes que les régions administratives.

# ## Charger les librairies

# In[1]:


import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from cycler import cycler


# ## Téléchargement des données

# In[20]:


url_hosp = "https://msss.gouv.qc.ca/professionnels/statistiques/documents/covid19/COVID19_Qc_HistoHospit.csv"
data_hosp = pd.read_csv(url_hosp)
data_hosp.to_csv(r'data/raw/COVID19_Qc_HistoHospit.csv')
data_hosp


# ## Nettoyage et manipulations des données

# In[21]:


# uniformiser le nom de colonne "date" qui avait une majuscule dans ce jeu (était pour uniformiser avec le jeu sur la vaccinnation au départ)
data_hosp = data_hosp.rename(columns = {"Date": "date"})

# éliminer les données d'avant 2021 (donc pour conserver période après début de la vaccination)
data_hosp = data_hosp[data_hosp["date"].str.contains("2020") == False]

# ordonner en ordre croissant de date
data_hosp = data_hosp.sort_values("date", ascending=True)

# éliminer les colonnes "hors soins intensifs" pour ne conserver que les colonnes "soins intensifs" et total", en plus de la date
# source code : https://stackoverflow.com/questions/19071199/drop-columns-whose-name-contains-a-specific-string-from-pandas-dataframe
data_hosp = data_hosp[data_hosp.columns.drop(list(data_hosp.filter(regex='Hsi')))]
data_hosp


# ### Séparer les régions
# 
# RSS06 : Montréal  
# RSS14 : Lanaudière  
# RSS11 : Gaspésie

# In[22]:


# conserver seulement les colonnes de la région, en plus de la date
data_hosp_06 = data_hosp.loc[:, data_hosp.columns.str.contains("RSS06") | (data_hosp.columns == "date")]
data_hosp_14 = data_hosp.loc[:, data_hosp.columns.str.contains("RSS14") | (data_hosp.columns == "date")]
data_hosp_11 = data_hosp.loc[:, data_hosp.columns.str.contains("RSS11") | (data_hosp.columns == "date")]


# ### Ajouter proportions soins intensifs vs total hospitalisations

# In[23]:


data_hosp_06 = data_hosp_06.assign(Proportion_Si = data_hosp_06["ACT_Si_RSS06"] / data_hosp_06["ACT_Total_RSS06"] * 100)
data_hosp_14 = data_hosp_14.assign(Proportion_Si = data_hosp_14["ACT_Si_RSS14"] / data_hosp_14["ACT_Total_RSS14"] * 100)
data_hosp_11 = data_hosp_11.assign(Proportion_Si = data_hosp_11["ACT_Si_RSS11"] / data_hosp_11["ACT_Total_RSS11"] * 100)
data_hosp_06


# ### Données sur la population
# 
# Source : [Banque de données des statistiques officielles sur le Québec](https://bdso.gouv.qc.ca/pls/ken/ken213_afich_tabl.page_tabl?p_iden_tran=REPERE9N6Q8436388736645748$%7C0&p_lang=1&p_m_o=ISQ&p_id_ss_domn=986&p_id_raprt=3987#tri_tertr=06&tri_pop=30)

# In[24]:


# population de nos 3 régions par 100 000 habitants
pop2021_06 = 2024806 / 100000
pop2021_14 = 535132 / 100000
pop2021_11 = 92015 / 100000

# ajouter colonne de la proportion aux soins intensifs par 100 000 habitants
data_hosp_06 = data_hosp_06.assign(Proportion_Pop_Si = data_hosp_06["ACT_Si_RSS06"] / pop2021_06)
data_hosp_11 = data_hosp_11.assign(Proportion_Pop_Si = data_hosp_11["ACT_Si_RSS11"] / pop2021_11)
data_hosp_14 = data_hosp_14.assign(Proportion_Pop_Si = data_hosp_14["ACT_Si_RSS14"] / pop2021_14)

# ajouter colonne de la proportion des hospitalisations totales par 100 000 habitants
data_hosp_06 = data_hosp_06.assign(Proportion_Pop_Total = data_hosp_06["ACT_Total_RSS06"] / pop2021_06)
data_hosp_11 = data_hosp_11.assign(Proportion_Pop_Total = data_hosp_11["ACT_Total_RSS11"] / pop2021_11)
data_hosp_14 = data_hosp_14.assign(Proportion_Pop_Total = data_hosp_14["ACT_Total_RSS14"] / pop2021_14)
data_hosp_14


# ## Visualisations

# In[32]:


# sources code : 
# https://matplotlib.org/stable/tutorials/introductory/customizing.htmlimport matplotlib.pyplot as plt
# https://pandas.pydata.org/pandas-docs/version/0.23/generated/pandas.DataFrame.plot.html

# paramètres par défaut pour tous nos graphiques
plt.rcParams["figure.figsize"] = (12, 4)
plt.rcParams["axes.grid"] = True
plt.rcParams["figure.titleweight"] = "bold"
mpl.rcParams['axes.prop_cycle'] = cycler(color=['r', 'g', 'b', 'y'])
mpl.rcParams["lines.linewidth"] = 1
mpl.rcParams["xtick.major.size"] = 1
#plt.rcParams.keys()
#mpl.rcParams.keys()


# ### Province du Québec au complet

# In[33]:


df_hosp.plot(x= "date", y="ACT_Total_RSS99", title="Nombre d'hospitalisations (Québec)")
df_hosp.plot(x= "date", y="ACT_Si_RSS99", title="Nombre d'hospitalisations aux soins intensifs (Québec)")


# ### Proportion de personnes aux soins intensifs (par région)

# In[27]:


#data_hosp_11.plot(x= "date", y="Proportion_Pop_Si", title="Nombre de personnes aux soins intensifs (par 100 000 habitants)")

date = data_hosp_06["date"]
RSS06 = data_hosp_06["Proportion_Pop_Si"]
RSS11 = data_hosp_11["Proportion_Pop_Si"]
RSS14 = data_hosp_14["Proportion_Pop_Si"]

plt.plot(date, RSS06, label="Montréal")
plt.plot(date, RSS11, label="Gaspésie")
plt.plot(date, RSS14, label="Lanaudière")

plt.xlabel("Date")
plt.ylabel("Nombre par 100 000 habitants")
plt.title("Proportion de personnes aux soins intensifs (par région)")
plt.xticks([1, 60, 120, 180, 240, 300, 360, 420, 480, 540, 600, 660])
plt.xticks(rotation="vertical")
plt.legend()
plt.grid(False)
plt.show()


# - Proportionnellement à la population, on constate qu'il y a environ le double de personnes aux soins intensifs à Montréal vs les régions.

# ### Proportion de personnes hospitalisées (par région)

# In[28]:


date = data_hosp_06["date"]
RSS06t = data_hosp_06["Proportion_Pop_Total"]
RSS11t = data_hosp_11["Proportion_Pop_Total"]
RSS14t = data_hosp_14["Proportion_Pop_Total"]

plt.plot(date, RSS06t, label="Montréal")
plt.plot(date, RSS11t, label="Gaspésie")
plt.plot(date, RSS14t, label="Lanaudière")

plt.xlabel("Date")
plt.ylabel("Nombre par 100 000 habitants")
plt.title("Proportion de personnes hospitalisées (par région)")
plt.xticks([1, 60, 120, 180, 240, 300, 360, 420, 480, 540, 600, 660])
plt.xticks(rotation="vertical")
plt.legend()
plt.grid(False)
plt.show()


# - Encore ici, Montréal semble avoir environ le double de personnes hospitalisées proportionnellement à la population.
# - Les vagues d'hospitalisations en Gaspésie sont souvent décalées, mais surtout en dents de scie (beaucoup de variations).
# 

# ### Proportion de personnes hospitalisées qui sont aux soins intensifs (par région)

# In[29]:


date = data_hosp_06["date"]
RSS06tsi = data_hosp_06["Proportion_Si"]
RSS11tsi = data_hosp_11["Proportion_Si"]
RSS14tsi = data_hosp_14["Proportion_Si"]

plt.plot(date, RSS06tsi, label="Montréal")
plt.plot(date, RSS11tsi, label="Gaspésie")
plt.plot(date, RSS14tsi, label="Lanaudière")

plt.xlabel("Date")
plt.ylabel("Pourcentage")
plt.title("Proportion de personnes hospitalisées qui sont aux soins intensifs (par région)")
plt.xticks([1, 60, 120, 180, 240, 300, 360, 420, 480, 540, 600, 660])
plt.xticks(rotation="vertical")
plt.legend()
plt.grid(False)
plt.show()


# - À quelques occasions, on remarque que 100% des personnes hospitalisées sont aux soins intensifs dans Lanaudière et en Gaspésie. 
# - Accès aux hôpitaux en région?

# ## Améliorations
# 
# - Peaufiner la présentation des graphiques pour mieux voir
# - Comparer avec le nombre de morts
# - Analyser en fonction du nombre d'hôpitaux dans la région
# - Comparer avec les phases de déconfinement
# - Est-ce que les personnes étaient vaccinées ou pas?
# - Date élection, d'événements particuliers, congés favorables aux rassemblements

# In[ ]:




