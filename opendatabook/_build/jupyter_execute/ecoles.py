#!/usr/bin/env python
# coding: utf-8

# # Afficher sur une carte les écoles publiques québécoises
# 
# Les données sont disponibles [ici](https://www.donneesquebec.ca/recherche/dataset/localisation-des-etablissements-d-enseignement-du-reseau-scolaire-au-quebec/resource/7c5a3d49-2ed1-4b86-8d14-4ae0d3668d89).
# 
# Malheureusement c'est un fichier, il faut le télécharger, il n'y a pas une API.
# 
# 
# Nous allons utiliser la librairie `json`:

# In[1]:


import json


# J'ai mis le fichier dans le dossier `ecolesquebec`. Nous pouvons parser le dictionnaire à l'aide de la fonction `json.load`:

# In[2]:


fichier = 'ecolesquebec/PPS_Public_Ecole.geojson'

with open (fichier,'r') as file:
    data = json.load(file)


# Nous pouvons maintenant regarder à quoi ressemblent les données:

# In[3]:


data.keys()


# In[4]:


data['features'][0]


# In[5]:


for ecole in data['features']:
    print(ecole['properties']['NOM_OFFCL_ORGNS'])


# In[6]:


typesecole=[]
for ecole in data['features']:
    typesecole.append(ecole['properties']['ORDRE_ENS'])
set(typesecole)    


# In[7]:


len(data['features'])


# In[8]:


import folium


# In[9]:


m = folium.Map([0, 0], zoom_start=2)

for ecole in data['features']:
   
    folium.Marker([ecole['geometry']['coordinates'][0], ecole['geometry']['coordinates'][1]], popup=ecole['properties']['NOM_COURT_ORGNS']).add_to(m)
m


# Ops! J'ai inversé latitude et longitude!

# In[10]:


m = folium.Map([0, 0], zoom_start=2)

for ecole in data['features']:
   
    folium.Marker([ecole['geometry']['coordinates'][1], ecole['geometry']['coordinates'][0]], popup=ecole['properties']['NOM_COURT_ORGNS']).add_to(m)
m


# In[11]:


m = folium.Map([47, -65], zoom_start=5)

for ecole in data['features']:
    if 'Primaire' in ecole['properties']['ORDRE_ENS']:   
        folium.Marker([ecole['geometry']['coordinates'][1], ecole['geometry']['coordinates'][0]], popup=ecole['properties']['NOM_COURT_ORGNS']).add_to(m)
m


# In[12]:


data['features'][0]


# ## Coupler les données avec d'autres données
# 
# Je vais prendre en cosidération les données sur les "lieux habités" : https://www.donneesquebec.ca/recherche/dataset/lieu-habite/resource/34a334bd-b56d-4314-aadc-6fdd5a65c2a4

# In[13]:


fichier = 'ecolesquebec/resultat_TQ.geojson'

with open (fichier,'r') as file:
    datalieux = json.load(file)


# In[14]:


datalieux['features'][0]['geometry']


# In[15]:


len(datalieux['features'])


# Je vais écrire une fonction pour calculer la distance entre deux points géographiques

# In[16]:


from math import radians, sin, cos, acos

def distance(a,b,c,d):
    slat = radians(float(a))
    slon = radians(float(b))
    elat = radians(float(c))
    elon = radians(float(d))

    dist = 6371.01 * acos(sin(slat)*sin(elat) + cos(slat)*cos(elat)*cos(slon - elon))
    return dist



# In[17]:


a= datalieux['features'][259]['geometry']['coordinates'][1]
b=datalieux['features'][259]['geometry']['coordinates'][0]
c=datalieux['features'][0]['geometry']['coordinates'][1]
d=datalieux['features'][0]['geometry']['coordinates'][0]


# In[18]:


distance(a,b,c,d)


# In[ ]:





# In[19]:


donneesecoles=[]
for lieu in datalieux['features']:
    llat = lieu['geometry']['coordinates'][1]
    llong = lieu ['geometry']['coordinates'][0]
    
    mindist=1000
    for ecole in data['features']:
        lat = ecole['geometry']['coordinates'][1]
        long = ecole['geometry']['coordinates'][0]
        dist = distance(lat,long,llat,llong)
        if dist < mindist:
            mindist = dist
            monecole=ecole
    lieu.update({'ecole':monecole, 'distanceecole':mindist})
    donneesecoles.append(lieu)


# In[20]:


donneesecoles[0]


# In[21]:


res = sorted(donneesecoles, key = lambda ele: ele['distanceecole'])


# In[22]:


res[-5]


# In[23]:


m = folium.Map([47, -65], zoom_start=5)

for lieu in donneesecoles:
    if lieu['distanceecole'] > 50:
        icon=folium.Icon(color='red')
        folium.Marker([lieu['geometry']['coordinates'][1], lieu['geometry']['coordinates'][0]], popup=lieu['properties']['nomcartrou']+'\n'+lieu['ecole']['properties']['NOM_COURT_ORGNS'],icon=icon).add_to(m)

        icon=folium.Icon(color='blue')
        folium.Marker([lieu['ecole']['geometry']['coordinates'][1], lieu['ecole']['geometry']['coordinates'][0]], popup=lieu['ecole']['properties']['NOM_COURT_ORGNS']+lieu['ecole']['properties']['ORDRE_ENS'],icon=icon).add_to(m)
m


# In[ ]:




