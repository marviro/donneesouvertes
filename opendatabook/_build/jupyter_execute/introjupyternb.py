#!/usr/bin/env python
# coding: utf-8

# # Les jupyter-notebooks
# 
# <h1> Les jupyter-notebooks</h1>
# 
# Un jupyter-notebook est un environnement d'écriture qui permet de combiner du texte et du code. Dans cet environnement il est possible d'exécuter directement le code. 
# 
# [Le projet Jupyter](https://jupyter.org/) permet d'utiliser plusieurs langages informatiques. Dans le cadre du séminaire nous allons nous limiter à `python`.
# 
# 
# Toute la documentation ets disponible [ici](https://jupyter-notebook.readthedocs.io/en/stable/).
# 
# 

# ## Installer
# 
# Pour faire fonctionner jupyter-notebook vous avez donc besoin d'avoir `python` sur votre machine.
# 
# Vous pouvez donc:
# 
# 1. Installer [`python`](https://www.python.org/downloads/)
# 2. Installer [`pip`](https://pip.pypa.io/en/stable/installation/)
# 3. Installer [`jupyter-notebook`](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html)
# 
# Ou alors installer un paquet qui a déjà tout, par exemple [Anaconda](https://www.anaconda.com/products/individual)
# 
# 
# 

# ## Blocs
# 
# Un jupyter-notebook est structuré en blocs. Chaque bloc peut être du code ou du texte "normal".
# 
# Le texte peut (doit) être écrit en markdown. [Voici une rapide introduction à markdown](https://www.markdowntutorial.com)
# 
# Regardons maintenant un bloc de code:

# In[1]:


machaine = 5
print(machaine)


# On vient de créer notre premier algorithme... 
# 
# Il est intéressant de voir que le premier algorithme est toujours un ["Hello world"](https://fr.wikipedia.org/wiki/Hello_world)... des idées à ce propos? 
# 
# Essayons quelque chose d'autre:

# In[2]:


machaine2= machaine+' et coucou!'
print(machaine2)


# In[8]:


a = 2
b = 3
print(a+b)


# Sympa, n'est-ce pas?
