#!/usr/bin/env python
# coding: utf-8

# # Une très très courte introduction à Python
# 
# Cette introduction est conçue pour être présentée en un cours (3h) à des étudiant.e.s de littérature, sans aucune connaissance informatique. Pour une introduction plus approfondie cf [l'introduction à python de Romain Tavenard](https://rtavenar.github.io/poly_python/content/intro.html).
# 
# - Un langage de haut niveau: donc proche du langage naturel.
# 
# Commençons avec notre premier programme:
# 
# 
# 

# In[1]:


print("Bonjour")


# Pour faire la même chose en Assembly - un des langages de programmation de niveau plus bas - il aurait fallu écrire quelque chose du type):
# 
# ```
# section .text
#    global _start    
#     
# _start:             
#    mov  edx,len     
#    mov  ecx,msg     
#    mov  ebx,1       
#    mov  eax,4       
#    int  0x80        
#     
#    mov  eax,1       
#    int  0x80        
# 
# section .data
# msg db 'Hello, world!', 0xa  
# len equ $ - msg     
# 
# ```

# ## Variables
# 
# Une variable est un objet auquel on attribue une certaine valeur. Il y a plusieurs types en python, pour l'instant nous considérérons
# 
# 1. la chaîne de caractères (string) : `str`
# 2. le nombre naturel (integer): `int`
# 3. le booléen (vrai ou faux): `bool`
# 
# Par exemple:

# In[2]:


mon_texte = 'Voici mon beau texte' #ça c'est un commentaire. Python n'interpète pas ce qui suit un #

mon_nombre = 2 # voici une variable qui a un type  "int"

mon_operateur_booleen = True


# In[3]:


print(str(mon_nombre)+'lala')


# Si je veux savoir le type de chaque variable je peux faire:

# In[4]:


print(type(mon_texte))
print(type(mon_nombre))
print(type(mon_operateur_booleen))


# Je peux faire des opératoins avec les variables. Par exemple je peux concatener deux chaînes de caractères ou additionner deux nombres.

# In[5]:


mon_texte + ' - et voici sa continuation'


# In[6]:


mon_nombre * 3


# ## Structures conditionnelles
# 
# On vérifie si une condition est remplie pour réaliser une opération.
# 
# Par exemple: s'il pleut, prend ton parapluie, s'il ne pleut pas, laisse-le à la maison.

# In[7]:


il_pleut = True


if il_pleut:
   print('Prends ton parapluie!')
   if 1>2:
       print('et 1 et plus grand que 0')
       
else:
   print('Laisse ton parapluie à la maison!')
       


# **Attention à l'indentation! elle est interpétée par python!**

# Nous pouvons définir une fonction:

# In[8]:


def parapluie(x):
    if x:
        print('Prends ton parapluie!')
    else:
        print('Laisse ton parapluie à la maison!')
    


# Et ensuite lui passer des valeurs:

# In[9]:


il_pleut = True
il_pleut_2 = False

parapluie(il_pleut)

parapluie(il_pleut_2)


# In[10]:


parapluie(True)


# On peut attribuer de nouveaux valeurs à la même variable:

# In[11]:


il_pleut = True
parapluie(il_pleut)

il_pleut = False
parapluie(il_pleut)


# ## Les listes
# 
# Une liste est une collection ordonnée de valeurs. Elle s'exprime avec des crochets `[]`. À l'intérieur des crochets il y aura les valeurs séparées par des virgules. Nous pouvons avoir des listes de chaînes de caractères, de nombres, de booleens... ou aussi mélangées
# 
# Par exemple:

# In[12]:


ma_liste = [1,3,5,8,9] #une liste de nombres
ma_liste_cc = ['Jean','Marie','Pierre','Délphine','Joseph']
ma_liste_miste = [1, 'Marie', '4 pommes', True]


# Maintenant nous pouvons parcourir une liste et afficher chaque valeur. On utilisera une **boucle**:

# In[13]:


for i in ma_liste:
   
    print(i)


# In[14]:


for valeur in ma_liste_cc:
    print(valeur)


# In[15]:


for position, valeur in enumerate(ma_liste_cc):
    print(position)
    print(valeur)


# In[16]:


len(ma_liste_cc)


# **Attention, le premier élément est l'élément `0` (et non `1`)!**
# 
# Le dernier élément est le `-1`

# In[17]:


ierrema_liste_cc[-1]


# ## Dictionnaires
# 
# Admettons d'avoir des informations sur une personne:
# 
# - prénom
# - nom
# - téléphone

# In[57]:


info_pers_1 = {'prenom' : 'André', 'nom' : 'Bouchard', 'tel' : '514 000 000' }


# Accédons maintenant à ces informations:

# In[58]:


print(info_pers_1['prenom'])


# In[59]:


info_pers_1['tel']


# Et si on en a plusieurs? On peut faire une liste de dictionnaires:

# In[60]:


list_dict = []
list_dict.append(info_pers_1)

info_pers_2 = {'prenom' : 'Josephine', 'nom' : 'Autel', 'tel' : '438 111 111' }

list_dict.append(info_pers_2)


# In[68]:


list_dict


# Et maintenant on fait une boucle:

# In[69]:


for personne in list_dict:
   # print(personne['prenom'] + ' ' + personne['nom'] + ' ' + personne['tel']) # le + ' ' sert juste à ajouter un espace blanc 
    print(personne['tel'])
    if '514' in personne['tel']:
        print('Montréal')


# In[78]:


list_dict[1]['prenom']


# In[70]:


dict_aristote = {'nom': 'Aristote', 'ville de naissance': 'Stagire', 'naissance' : '-384', 'mort': '-322', 'ville de mort': 'Calcis', 'oeuvres': [{'titre': 'Métaphysique', 'sujet' : 'principes premiers'},{'titre': 'Physique', 'sujet' : 'la nature'}, ]}


# In[71]:


dict_aristote['nom']


# In[72]:


dict_aristote['oeuvres']


# In[73]:


for oeuvre in dict_aristote['oeuvres']:
    print(oeuvre['titre'])
    print(oeuvre['sujet'])


# In[74]:


dict1 = {'titre': 'mon titre', 'texte' : 'letexteduroman'}

dict1.update({'date': '1962'})



# In[76]:


dict1


# In[78]:


dict1.update({'titre': 'L\'avalée'})


# In[79]:


dict1


# In[80]:


dict2 = {'titre': "Va savoir", 'texte': 'letexteduroman', 'date': '1972'}


# In[81]:


maliste= [dict1]


# In[82]:


maliste[-1]['titre']


# In[83]:


maliste.append(dict2)


# In[84]:


maliste[1]['titre']


# In[87]:


dict1.keys()


# In[105]:


dict1.update({'titre': 'blabla', 'date': '182'})


# In[106]:


dict1['titre2']


# In[ ]:




