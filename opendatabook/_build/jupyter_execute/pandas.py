#!/usr/bin/env python
# coding: utf-8

# # Utiliser pandas pour lire des csv
# 
# On pourrait dans ce cas aussi utiliser l'api...
# 
# https://www.donneesquebec.ca/recherche/api/3/action/datastore_search?resource_id=0fab8d3e-7b40-4eb9-a212-286e5c404a24&limit=5
# 
# Mais complicons-nous un peu la vie.
# 

# In[1]:


import pandas as pd

df = pd.read_csv('publications/extract_notice_scp_20230108.csv', encoding = "ISO-8859-1", sep=',')


# In[2]:


df


# In[3]:


df.keys()


# In[4]:


dccnom=df['DC_CONTRIBUTOR__NOM']


# In[5]:


dccnom.tolist()


# In[6]:


liste_noms=df['DC_CONTRIBUTOR__NOM'].tolist()


# Maintenant selectionnons les lignes qui ont comme contrinuteur "Tremblay"

# In[7]:


df.loc[df['DC_CONTRIBUTOR__NOM'] == 'Tremblay']


# In[8]:


df.loc[(df['DC_CONTRIBUTOR__PRENOM'] == 'Tremblay') & (df['DC_CONTRIBUTOR__PRENOM'] == 'Carole')]


# In[11]:


df.keys()


# In[12]:


df.loc[(df['DC_CONTRIBUTOR__NOM'] == 'Tremblay') & (df[' DC_CONTRIBUTOR__PRENOM'].str.contains('Claude'))]


# In[55]:


df.loc[(df['DC_CONTRIBUTOR__NOM'] == 'Tremblay') & (df[' DC_CONTRIBUTOR__PRENOM'].str.contains('Claude'))]['DC_TITLE']


# In[56]:


df.head()


# In[57]:


df.head(30)


# In[65]:


df.loc[((df['DC_CREATOR__NOM'].notnull()) & (df['DC_CONTRIBUTOR__NOM'] == 'Tremblay') & ( (df[' DC_CONTRIBUTOR__PRENOM'].str.contains('Carole')) | (df[' DC_CONTRIBUTOR__PRENOM'].str.contains('Claude'))))]


# In[ ]:




