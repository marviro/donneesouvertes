## Licences

**Libre c'est libre!**



## Choisir une licence

[Creative commons](https://creativecommons.org/share-your-work/)

[Data Europa licensing assistant](https://data.europa.eu/en/training/licensing-assistant)


[ODC](https://opendatacommons.org/licenses/)


[Open data licensing selon la commision européenne](https://data.europa.eu/en/academy/open-data-licensing)

<iframe scrolling="no" class="iFrame-vimeo-iframe" src="https://www.youtube.com/embed/_wv9QflBJIY&amp;list=PLT5rARDev_rnG9rj7hZG0hALPWQS8mLGA&amp;index=4" data-height-large="508" data-height-medium="350" data-height-small="250" data-width-large="905" data-width-medium="500" data-width-small="300" style="width: 905px; height: 508px;">


