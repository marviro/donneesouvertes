## Les principes des données ouvertes

- complètes
- primaires
- actuelles
- accessibles
- utilisables par des machines
- non discriminantes
- libres

[Cf ici](https://opendatapolicies.org/guidelines/)

## Problèmes

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/lang/en/ben_wellington_how_we_found_the_worst_place_to_park_in_new_york_city_using_big_data" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>




- données accessibles
- données bien structurées
  - accessibles comment? On va chercher un disque si on demande?
  - pdf
  - csv
  - api
  - structurer = concevoir des paradigmes épistémologiques
  - standardisées ou pas? normalisation


## Quels formats?

- csv
- xml
- json
- ...
