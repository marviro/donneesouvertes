# Les données ouvertes


## Que sont les données ouvertes?

:::{admonition} Définition
Données numériques accessibles, utilisables, documentées.
:::

- Production de connaissance et importance de l'accessibilité. 

- Accessibilité non seulement de documents mais aussi de données

- Le rêve du savoir.

## Documents et données


## Qui produit des données ouvertes?

- instances publiques
  - gouvernements
  - institutions publiques
  - ...
- institution de recherche (publiques ou privées)
- autres (individus, instances privées...)


## La question éthique

- dans le domaine des données publiques
- dans le domaine des données de la recherche


La question de l'accès libre

## Les caractéristiques des données ouvertes

- accessibles
- pertinentes
- documentées
- structurées
- visibles
- liées ?

ou, selon la [Open Knowledge foundation](https://opendatahandbook.org/guide/fr/what-is-open-data/):


- *Disponibilité et accès*: Les données doivent être disponible dans leurs ensembles et pour un coût raisonnable de reproduction, de préférence téléchargeable sur internet. Les données doivent êtes aussi disponibles dans un format pratique et sous forme modifiable.
- *Réutilisation et Redistribution* Les données doivent être disponibles sous une license autorisant la réutilisation et la redistribution incluant le croisement avec d’autres ensembles de données.
- *Participation universelle:* tout le monde doit être en mesure d’utiliser, de réutiliser et redistribuer - il ne devrait y avoir aucune discrimination contre les champs de l’activité ou contre des personnes ou des groupes. Pour exemple, des restrictions ‘non-commercial’ qui empêcheraient les utilisations ‘commercial’ ou des restrictions d’utilisation à certaines fins (par exemple seulement dans l’éducation) ne sont pas autorisés.




